-mthumb -mcpu=cortex-m3 
-DUSE_STDPERIPH_DRIVER -DSTM32F4XX -DUSE_USB_OTG_FS -DMEDIA_USB_KEY 
-I../../../../../Libraries/CMSIS/Include 
-I../../../../../Libraries/CMSIS/ST/STM32F4xx/Include 
-I../../../../../Libraries/STM32F4xx_StdPeriph_Driver/inc 
-I"../..\..\..\..\Libraries\STM32_USB_OTG_Driver\inc" 
-I../../../inc 
-I../../../../../Libraries/STM32_USB_HOST_Library/Core/inc 
-I../../../../../Libraries/STM32_USB_HOST_Library/Class/MSC/inc 
-I"../../..\..\..\Utilities\Third_Party\fat_fs\inc" 
-I"../..\..\..\..\Utilities\STM32F4-Discovery" 
-O0 -ffunction-sections -fdata-sections -g -Wall -mcpu=cortex-m4 -Os

USE_STDPERIPH_DRIVER, USE_USB_OTG_FS, STM32F4XX, MEDIA_USB_KEY, __FPU_PRESENT=0

-c --cpu Cortex-M4 
-D__EVAL -D__MICROLIB 
-g -O3 --apcs=interwork --split_sections 
-I..\inc -I..\..\..\Libraries\CMSIS\ST\STM32F4xx\Include 
-I..\..\..\Libraries\CMSIS\Include 
-I..\..\..\Utilities\Third_Party\fat_fs\inc 
-I..\..\..\Libraries\STM32F4xx_StdPeriph_Driver\inc 
-I..\..\..\Libraries\STM32_USB_HOST_Library\Core\inc 
-I..\..\..\Libraries\STM32_USB_HOST_Library\Class\MSC\inc 
-I..\..\..\Libraries\STM32_USB_OTG_Driver\inc 
-I..\..\..\Utilities\STM32F4-Discovery 
-I "g:\Keil\ARM\INC" 
-I "g:\Keil\ARM\INC\ST\STM32F4xx" 

-DUSE_STDPERIPH_DRIVER -DUSE_USB_OTG_FS -DSTM32F4XX -DMEDIA_USB_KEY -D__FPU_PRESENT="0" 
-o ".\MEDIA_USB_KEY\*.o" --omf_browse ".\MEDIA_USB_KEY\*.crf" --depend ".\MEDIA_USB_KEY\*.d"

--cpu Cortex-M4 *.o --library_type=microlib --strict --scatter ".\MEDIA_USB_KEY\MEDIA_USB_KEY.sct" 
--autoat --summary_stderr --info summarysizes --map --xref --callgraph --symbols 
--info sizes --info totals --info unused --info veneers 
 --list ".\MEDIA_USB_KEY.map" 
-o ".\MEDIA_USB_KEY\MEDIA_USB_KEY.axf" 

-I../../../../../Libraries/CMSIS/ST/STM32F4xx/Include
-I../../../../../Libraries/STM32F4xx_StdPeriph_Driver/inc 
-I../..\..\..\..\Libraries\STM32_USB_OTG_Driver\inc 
-I../../../inc 
-I../../../../../Libraries/STM32_USB_HOST_Library/Core/inc 
-I../../../../../Libraries/STM32_USB_HOST_Library/Class/MSC/inc 
-I../../..\..\..\Utilities\Third_Party\fat_fs\inc 
-I../..\..\..\..\Utilities\STM32F4-Discovery -O0 
